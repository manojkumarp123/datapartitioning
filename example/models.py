"""Data Partitioning at Model Level. The way objects are accessed from business logic(views) remains the same."""
from django.db import models
from django.contrib.auth.models import User
from polymorphic.models import PolymorphicModel, PolymorphicManager


class QuestionResponseManager(PolymorphicManager):

    def filter(self, **kwargs):
        if "user" not in kwargs:
            raise Exception("DataPartitioning RequiredField Error: user argument not specified.")
        model = self._get_partition_model(user=kwargs["user"])
        return model.objects.filter(**kwargs)

    def create(self, **kwargs):
        if "user" not in kwargs:
            raise Exception("DataPartitioning RequiredField Error: user argument not specified.")
        model = self._get_partition_model(user=kwargs["user"])
        return model.objects.create(**kwargs)

    def _get_partition_model(self, user):
        if user.id > 0 and user.id <= 10:
            model = QuestionResponsePartition1
        elif user.id > 10 and user.id <= 20:
            model = QuestionResponsePartition2
        elif user.id > 20 and user.id <= 30:
            model = QuestionResponsePartition3
        else:
            model = QuestionResponsePartition4
        return model


class QuestionResponse(PolymorphicModel):
    data = models.CharField(max_length=200)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    objects = QuestionResponseManager()


class QuestionResponsePartition4Manager(PolymorphicManager):
    pass


class QuestionResponsePartition4(QuestionResponse):
    objects = QuestionResponsePartition4Manager()


class QuestionResponsePartition3Manager(PolymorphicManager):
    pass


class QuestionResponsePartition3(QuestionResponse):
    objects = QuestionResponsePartition3Manager()


class QuestionResponsePartition2Manager(PolymorphicManager):
    pass


class QuestionResponsePartition2(QuestionResponse):
    objects = QuestionResponsePartition2Manager()


class QuestionResponsePartition1Manager(PolymorphicManager):
    pass


class QuestionResponsePartition1(QuestionResponse):
    objects = QuestionResponsePartition1Manager()
