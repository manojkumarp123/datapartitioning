"""test.py."""
import random
import string

from collections import defaultdict

from django.contrib.auth.models import User
from django.test import TestCase

from .models import (
    QuestionResponse, QuestionResponsePartition1, QuestionResponsePartition2,
    QuestionResponsePartition3, QuestionResponsePartition4
)


class DataPartitionTestCase(TestCase):
    """data partition test case."""

    def setUp(self):
        """"Trigger the create orm method. monitor the count of inserts in each partition."""
        self.partitions_sum = defaultdict(int)
        self.user_question_responses = defaultdict(dict)
        for user_index in range(50):
            username = self._get_random_word()
            user = User.objects.create_user(username=username)
            for question_resposne_index in range(random.randint(1, 10)):

                # partition
                if user.id <= 10:
                    partition = 1
                elif user.id > 10 and user.id <= 20:
                    partition = 2
                elif user.id > 20 and user.id <= 30:
                    partition = 3
                else:
                    partition = 4
                # count
                self.partitions_sum[partition] += 1

                # Write using one query.
                with self.assertNumQueries(2):
                    question_response = QuestionResponse.objects.create(data=self._get_random_word(), user=user)

                self.user_question_responses[user][question_response.id] = question_response

    def test_data_in_partitions(self):
        """Actual data partition contains number of rows inserted."""
        total = 0

        question_response_data_partition_1_count = QuestionResponsePartition1.objects.count()
        total += question_response_data_partition_1_count
        self.assertEqual(self.partitions_sum[1], question_response_data_partition_1_count)

        question_response_data_partition_2_count = QuestionResponsePartition2.objects.count()
        total += question_response_data_partition_2_count
        self.assertEqual(self.partitions_sum[2], question_response_data_partition_2_count)

        question_response_data_partition_3_count = QuestionResponsePartition3.objects.count()
        total += question_response_data_partition_3_count
        self.assertEqual(self.partitions_sum[3], question_response_data_partition_3_count)

        question_response_data_partition_4_count = QuestionResponsePartition4.objects.count()
        total += question_response_data_partition_4_count
        self.assertEqual(self.partitions_sum[4], question_response_data_partition_4_count)

        self.assertEqual(sum(self.partitions_sum.values()), total)
        self.assertEqual(total, QuestionResponse.objects.count())

    def test_filter_num_queries(self):
        """Filter method fetches question response of users from appropriate partition using a single query."""
        for user in User.objects.all():
            with self.assertNumQueries(1):
                question_responses = QuestionResponse.objects.filter(user=user)
                for question_response in question_responses:
                    self.assertEqual(self.user_question_responses[user][question_response.id], question_response)

    def _get_random_word(self, length=8):
        return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))
